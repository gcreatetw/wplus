package com.gcreate.wplus.Tool;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class GenerateTxt {

    public static void writeTxtToFile(Activity mActivity, UserInfoModel userdata) {

        int REQUEST_CODE_CONTACT = 101;
        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

        for (String str : permissions) {
            if (mActivity.checkSelfPermission(str) != PackageManager.PERMISSION_GRANTED) {
                //申请权限
                mActivity.requestPermissions(permissions, REQUEST_CODE_CONTACT);
                return;
            } else {
                String path = Environment.getExternalStorageDirectory() + "/WPLUS";
                File files = new File(path);

                //  建立 WPLUS 的資料夾路徑
                if (!files.exists()) {
                    files.mkdirs();
                }
                String name = path + File.separator + "testerInfo-" + getCurrentDate() + ".txt";
                try {
                    OutputStreamWriter fw = new OutputStreamWriter(new FileOutputStream(name, true), StandardCharsets.UTF_16);
                    fw.write("姓名 : " + userdata.userName + '\n' +
                            "性別 : " + userdata.userGender + '\n' +
                            "出生日期 : " + userdata.userBirthday + '\n' +
                            "手提電話 : " + userdata.userCellphone + '\n' +
                            "微信帳號 : " + userdata.weChat + '\n' + '\n');
                    fw.flush();
                    fw.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }

    private static String getCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return sdf.format(new Date());
    }


    /**
     * 使用BufferedWriter进行文本内容的追加
     *
     * @param file
     * @param content
     */
    private void addTxtToFileBuffered(File file, String content) {
        //在文本文本中追加内容
        BufferedWriter out = null;
        try {
            //FileOutputStream(file, true),第二个参数为true是追加内容，false是覆盖
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true)));
            out.newLine();//换行
            out.write(content);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 使用FileWriter进行文本内容的追加
     *
     * @param file
     * @param content
     */
    private void addTxtToFileWrite(File file, String content) {
        FileWriter writer = null;
        try {
            //FileWriter(file, true),第二个参数为true是追加内容，false是覆盖
            writer = new FileWriter(file, true);
            writer.write("\r\n");//换行
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}