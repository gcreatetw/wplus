package com.gcreate.wplus.Tool;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import com.gcreate.wplus.view.DialogFragment_otherSetting;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class StorageDataMaintain {

    public static List<String> realPathList;
    public static List<String> uriPathList;
    public static List<String> otherSettingList;


    //------------------------------------------- init -------------------------------------------
    public static void initPathArray(Context context) {
        // 絕對路徑 Array
        realPathList = new ArrayList<>();
        realPathList.add(0, "");
        realPathList.add(1, "");
        realPathList.add(2, "");
        realPathList.add(3, "");

        // Uri Path
        uriPathList = new ArrayList<>();
        uriPathList.add(0, "");//商標
        uriPathList.add(1, "");//贊助商
        uriPathList.add(2, "");//推廣標籤圖片
        uriPathList.add(3, "");//閒置廣告

        // 地區 & 組別碼
        otherSettingList = new ArrayList<>();
        otherSettingList.add(0, "XXX");
        otherSettingList.add(1, "NNN");

        SharedPreferences.Editor realPathEditor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        realPathEditor.putString("ImageRealPathList", new Gson().toJson(realPathList));
        realPathEditor.apply();

        SharedPreferences.Editor uriPathEditor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        uriPathEditor.putString("UriPathList", new Gson().toJson(uriPathList));
        uriPathEditor.apply();

        SharedPreferences.Editor otherSettingEditor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        otherSettingEditor.putString("OtherSettingList", new Gson().toJson(otherSettingList));
        otherSettingEditor.apply();

    }

    //------------------------------------------- get  取得圖片絕對路徑 -------------------------------------------
    public static void getImageRealPath(Context context) {
        SharedPreferences sp = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE);
        String ListJson = sp.getString("ImageRealPathList", "Empty Array");
        if (!ListJson.isEmpty()) {
            Gson gson = new Gson();
            realPathList = gson.fromJson(ListJson, new TypeToken<List<String>>() {
            }.getType());
        }
    }

    //------------------------------------------- Save  圖片路徑 -------------------------------------------
    private static void saveImageRealPath(Context context) {
        if (realPathList != null) {
            SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
            editor.putString("ImageRealPathList", new Gson().toJson(realPathList));
            editor.apply();
            editor.commit();
        }
    }

    //------------------------------------------- Update 單一圖片路徑  -------------------------------------------
    public static void updateSingleImageRealPath(Context context, int pos, String imageRealPath) {
        if (realPathList != null && realPathList.size() > pos) {
            realPathList.remove(pos);
            realPathList.add(pos, imageRealPath);
            saveImageRealPath(context);
        }
    }

    //------------------------------------------- get  取得圖片Uri路徑 -------------------------------------------
    public static void getUriPath(Context context) {
        SharedPreferences sp = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE);
        String ListJson = sp.getString("UriPathList", "Empty Array");
        if (!ListJson.isEmpty()) {
            Gson gson = new Gson();
            uriPathList = gson.fromJson(ListJson, new TypeToken<List<String>>() {
            }.getType());
        }
    }

    //------------------------------------------- Save  圖片Uri路徑 -------------------------------------------
    public static void saveUriPath(Context context) {
        if (uriPathList != null) {
            SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
            editor.putString("UriPathList", new Gson().toJson(uriPathList));
            editor.apply();
            editor.commit();
        }
    }

    //------------------------------------------- Update 單一圖片Uri路徑  -------------------------------------------
    public static void updateSingleUriPath(Context context, int pos, String uriPath) {
        if (uriPathList != null && uriPathList.size() > pos) {
            uriPathList.remove(pos);
            uriPathList.add(pos, uriPath);
            saveUriPath(context);
        }
    }


    //--------------------------------------------  壓力值設定   --------------------------------------------------------
    public static int getPressureSettingValue(Context context) {
        return ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).getInt("pressureSettingValue", 0);
    }

    public static void setPressureSettingValue(Context context, int position) {
        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putInt("pressureSettingValue", position);
        editor.apply();
        editor.commit();
    }

    //--------------------------------------------  密碼設定   --------------------------------------------------------
    public static String getPassword(Context context) {
        return ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).getString("password", "wplus");
    }

    public static void setPassword(Context context, String password) {
        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putString("password", password);
        editor.apply();
        editor.commit();
    }

    //------------------------------------------- get  取得地區&組別碼 -------------------------------------------
    public static void getOtherSettingValue(Context context) {
        SharedPreferences sp = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE);
        String ListJson = sp.getString("OtherSettingList", "Empty Array");
        if (!ListJson.isEmpty()) {
            Gson gson = new Gson();
            otherSettingList = gson.fromJson(ListJson, new TypeToken<List<String>>() {
            }.getType());
        }
    }


    //------------------------------------------- Save 區&組別碼 -------------------------------------------
    private static void saveOtherSettingValue(Context context,List<String> list) {
        if (list != null) {
            SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
            editor.putString("OtherSettingList", new Gson().toJson(list));
            editor.apply();
            editor.commit();
        }
    }

    //------------------------------------------- Update 區&組別碼 -------------------------------------------
    public static void updateOtherSettingValue(Context context, int pos, String value) {
        if (otherSettingList != null && otherSettingList.size() > pos) {
            otherSettingList.remove(pos);
            otherSettingList.add(pos, value);
            saveOtherSettingValue(context,otherSettingList);
        }
    }

}
