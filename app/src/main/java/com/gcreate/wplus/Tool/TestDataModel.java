package com.gcreate.wplus.Tool;

import android.graphics.Bitmap;

public class TestDataModel {


    private final Bitmap pressureGraph;
    private final int testPressureArea;
    private final float testPressureIndex;
    private final float compression;


    public TestDataModel(Bitmap pressureGraph, int testPressureArea, float testPressureIndex, float compression) {
        this.pressureGraph = pressureGraph;
        this.testPressureArea = testPressureArea;
        this.testPressureIndex = testPressureIndex;
        this.compression = compression;
    }

    public Bitmap getPressureGraph() {
        return pressureGraph;
    }

    public int getTestPressureArea() {
        return testPressureArea;
    }

    public float getTestPressureIndex() {
        return testPressureIndex;
    }

    public float getCompression() {
        return compression;
    }
}
