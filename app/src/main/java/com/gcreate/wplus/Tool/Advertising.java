package com.gcreate.wplus.Tool;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

public class Advertising {
    //  廣告
    public Handler advertisingHandler(Context context, Context packageContext, Class<?> cls) {

        return new Handler(msg -> {
            Intent mIntent = new Intent(packageContext, cls);
            mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(mIntent);
            return true;
        });
    }
}
