package com.gcreate.wplus.Tool;

import android.app.Activity;
import android.graphics.pdf.PdfDocument;
import android.os.Environment;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class GeneratePDF {


    public static void generatePdf(ConstraintLayout constraintLayout,String fileName) {

        //  1.建立PdfDocument
        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(constraintLayout.getWidth(), constraintLayout.getHeight(), 1).create();
        //  2.建立新的page
        PdfDocument.Page page = document.startPage(pageInfo);
        //  3.canvas把当前画面画出来
        constraintLayout.draw(page.getCanvas());
        document.finishPage(page);

        Date calendar = Calendar.getInstance().getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("YYMMdd-HHmm", Locale.getDefault());
        String currentDateAndTime = sdf.format(calendar);

        //  4.資料儲存路徑
        //  儲存資料夾
        String documentPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/WPLUS/";
        /**  坐墊測試儀-檔案名稱分類
         * HKG-015-200803-1635
         * XXX - 地區分類               (香港 HKG / 上海 SHA / 廣州 CAN / 深圳 SZX)
         * NNN - 組別號                   (001 – 999 ( 999 個組別 ))
         * YYMMDD - 日期分類      (20 日7 月2020年 => YYMMDD = > 200720)
         * TTTT - 時間分類              (晚上8 點28分 => 20:28 =>2028)
         */
//        String fileName = "XXX-NNN-" + currentDateAndTime + ".pdf";
        String fileName2 = fileName +".pdf";
        String storagePath = documentPath + fileName2;

        File file = new File(storagePath);

        if (file.exists()) {
            file.delete();
        }
        try {
            document.writeTo(new FileOutputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        document.close();
    }

}
