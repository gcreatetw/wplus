package com.gcreate.wplus.Tool;

public class UserInfoModel {

    String userName;
    String userGender;
    String userBirthday;
    String userCellphone;
    String weChat;

    public UserInfoModel(String userName, String userGender, String userBirthday, String userCellphone, String weChat) {
        this.userName = userName;
        this.userGender = userGender;
        this.userBirthday = userBirthday;
        this.userCellphone = userCellphone;
        this.weChat = weChat;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(String userBirthday) {
        this.userBirthday = userBirthday;
    }

    public String getUserCellphone() {
        return userCellphone;
    }

    public void setUserCellphone(String userCellphone) {
        this.userCellphone = userCellphone;
    }

    public String getWeChat() {
        return weChat;
    }

    public void setWeChat(String weChat) {
        this.weChat = weChat;
    }

}
