package com.gcreate.wplus.BlueTooth;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gcreate.wplus.MainActivity;
import com.gcreate.wplus.Tool.StorageDataMaintain;
import com.gcreate.wplus.view.Activity_test;

import java.util.Map;


public class CreateWplusImage extends androidx.appcompat.widget.AppCompatImageView {

    private final Paint p = new Paint();
    private int[][] colorArray;
    private float startX = 0;
    private float startY = 0;
    private static final float rectSize = 1;
    private static final Bitmap bitmap = Bitmap.createBitmap(289, 241, Bitmap.Config.RGB_565);;
    private static final Canvas canvas = new Canvas(bitmap);

    public CreateWplusImage(@NonNull Context context) {
        super(context);
        init();
    }

    public CreateWplusImage(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CreateWplusImage(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void init() {
        // init data && convert to color array
        Log.d("benCreate","init");
        initDataArray();
        getBTData();
        increaseX(3);
        increaseY(3);
        dataConvertColor(rawData);
        functionA(rawData);
        p.setStyle(Paint.Style.FILL);
        p.setAntiAlias(true);

//        if (bitmap == null){
//            bitmap = Bitmap.createBitmap(289, 241, Bitmap.Config.RGB_565);
//            canvas = new Canvas(bitmap);
//        }
    }

    public Bitmap getCurCanvas() {
        // Bitmap.createBitmap(  寬, 高 , Bitmap.Config.ARGB_8888);
        // 初始 : X = 37，Y = 31
        // 放大 1 倍 : X  =  73，Y = 61
        // 放大 2 倍 : X  =  145，Y = 121
        // 放大 3 倍 : X  =  289，Y = 241
        Log.d("Ben","");
//        Bitmap bitmap = Bitmap.createBitmap(289, 241, Bitmap.Config.RGB_565);
//        Canvas canvas = new Canvas(bitmap);
        canvas.scale((float) 1, (float) 1);
        draw(canvas);
        canvas.save();
        return bitmap;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (int[] ints : colorArray) {
            for (int anInt : ints) {
                p.setColor(anInt);
                canvas.drawRect(startX, startY, startX + rectSize, startY + rectSize, p);
                startX += rectSize;
            }
            startX = 0;
            startY += rectSize;
        }
        startY = 0;

    }


    //----------------------------------------------------------------------------------------------------------------------------
    private static int[][] rawData;
    public static int dataMaxValue = 0;
    public static int getBTDataCount = 0;

    private void initDataArray() {

        rawData = new int[][]{
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        };
    }

    private void getBTData() {
        if (Activity_SearchBlueTooth.map.size() != 0) {
            Map<Integer, String> BTData = Activity_SearchBlueTooth.map;
            //  get  each row
            for (int i = 0; i < 31; i++) {
                String rowDataString = BTData.get(i);

                // split 4 unit and convert to Integer
                for (int j = 0; j < 37; j++) {
                    try {
                        // 藍芽收到的值
                        int valueFromBLE = Integer.valueOf(rowDataString.substring(4 * j, 4 * (j + 1)), 16);
                        if (j > 0 && j < 36 && (rawData[i][j - 1]) / 2 > valueFromBLE) {
                            //  當受到的值，比上一個值的一半還小，則判斷該數值異常，使用該值的上一個跟下一個值相加取平均
                            rawData[i][j] = rawData[i][j - 1];
                        } else {
                            rawData[i][j] = valueFromBLE;
                        }

                    } catch (Exception e) {
//                        Log.d(TAG, e.toString());
                    }

                    if (rawData[i][j] > dataMaxValue && getBTDataCount <= 5) {
                        dataMaxValue = rawData[i][j];
                        Activity_test.maxPressureModel.setMaxPressureValue(String.valueOf(dataMaxValue));
                    }
                }
            }
            getBTDataCount++;
        }
    }

    /*放大*/
    private void increaseX(int time) {
        for (int i = time; i > 0; i--) {
            increaseX(rawData);
        }
    }

    private void increaseX(int[][] rawData) {
        int[][] increaseX = new int[rawData.length][((rawData[0].length) * 2) - 1];

        for (int i = 0; i < rawData.length; i++) {
            for (int j = 0; j < ((rawData[i].length) * 2) - 1; j++) {

                if (j % 2 == 0) {
                    increaseX[i][j] = rawData[i][j / 2];
                } else {
                    try {
                        increaseX[i][j] = (rawData[i][(j - 1) / 2] + rawData[i][(j + 1) / 2]) / 2;
                    } catch (Exception e) {
                        //Log.e(TAG, e.toString());
                    }
                }
            }
        }
        CreateWplusImage.rawData = increaseX;
    }

    private void increaseY(int time) {
        for (int i = time; i > 0; i--) {
            increaseY(rawData);
        }
    }

    private void increaseY(int[][] rawDataArray) {

        int newY = ((rawDataArray.length) * 2) - 1;
        int[][] increaseY = new int[newY][rawDataArray[0].length];

        for (int i = 0; i < newY; i++) {
            for (int j = 0; j < rawDataArray[0].length; j++) {

                if (i == 0) {
                    increaseY[0][j] = rawDataArray[0][j];
                } else if (i % 2 == 0) {
                    increaseY[i][j] = rawDataArray[i / 2][j];
                } else {

                    try {
                        increaseY[i][j] = (rawDataArray[(i - 1) / 2][j] + rawDataArray[(i + 1) / 2][j]) / 2;
                    } catch (Exception e) {
//                        Log.e(TAG, e.toString());
                    }
                }
            }
        }
        rawData = increaseY;
    }

    //----------------------------計算 受壓面積 / 壓力指數 / 受壓單位--------------------------
    private void functionA(int[][] rawData) {
        // 資料個數.
        int totalPressureDataCount = 1; //總受壓面積
        int pressureAreaDataCount = 0; // 受壓面積
        int SumOfPressureAreaDataValue = 0;
        float pressureIndex;


        for (int[] rawDatum : rawData) {
            for (int i : rawDatum) {
                /* 總受壓面積 、指所有的壓力點總和之面積
                 *只要壓力值不是白色範圍內. 當作有受到壓力
                 * 假如資料集 37*31. 其中承受到壓力的點有10個. 那 "總受壓面積"  = 10
                 */
                if (i > (dataMaxValue / 25)) {
                    totalPressureDataCount++;
                }
                /*受壓面積  : 指超過設定高壓力值的壓力點總和之面積
                 * "高壓力值" = 量到的壓力最大值*(壓力值設定: 數值為0~100%).
                 * 舉例來說最大壓力值 =1000, 使用者設定壓力值設定為60%, 意即受壓面積是計算資料集裡數值超過600的資料個數
                 */
                if (i > (dataMaxValue / 100) * StorageDataMaintain.getPressureSettingValue(MainActivity.mContext)) {
                    pressureAreaDataCount = pressureAreaDataCount + 1;
                    SumOfPressureAreaDataValue = SumOfPressureAreaDataValue + i;
                }
            }
        }

        // 壓力指數 =  (受壓面積 / 總受壓面積) X 100%
        pressureIndex = ((float) (pressureAreaDataCount) / (float) (totalPressureDataCount)) * 100;
        Log.d("ben888","壓力指數 pressureIndex = " +pressureIndex);
        Log.d("ben888","受壓面積 pressureAreaDataCount = " +pressureAreaDataCount);
        //受壓度 =  指所有受壓面積的數值和/所有受壓面積的感應點數量
        float compression = 0;
        if (pressureAreaDataCount != 0) {
            compression = (float) (SumOfPressureAreaDataValue / pressureAreaDataCount);
        }
        Activity_test.maxPressureModel.setPressureArea(String.valueOf(pressureAreaDataCount));
        Activity_test.maxPressureModel.setPressureIndex(pressureIndex);
        Activity_test.maxPressureModel.setCompression(String.valueOf(compression));


    }

    //----------------------------------------------------------------------------------------------------------------------
    private void dataConvertColor(int[][] rawData) {

        colorArray = new int[rawData.length][rawData[0].length];

        for (int i = 0; i < colorArray.length; i++) {
            for (int j = 0; j < colorArray[i].length; j++) {

                // 初始壓力最大值若小於雜訊值.100 顯示白色
                if (dataMaxValue > 100) {
                    colorArray[i][j] = colorTable(rawData[i][j]);
                } else {
                    colorArray[i][j] = Color.parseColor("#ffffff");
                }

            }
        }

    }

    private int colorTable(int i) {
        int baseMagnification = dataMaxValue / 25;

        // 成圖時扣掉雜訊的值

        if (i >= 0 && i <= baseMagnification) {
            // NO.1
            return Color.parseColor("#ffffff");
        } else if (i > baseMagnification && i < baseMagnification * 2) {
            // NO.1.5
            return Color.parseColor("#BEE8F4");
        } else if (i >= baseMagnification * 2 && i < baseMagnification * 3) {
            // NO.2
            return Color.parseColor("#7bd1e9");
        } else if (i >= baseMagnification * 2 && i < baseMagnification * 4) {
            // NO.2.5
            return Color.parseColor("#6ECCDD");
        } else if (i >= baseMagnification * 4 && i < baseMagnification * 5) {
            // NO.3
            return Color.parseColor("#5cc7d0");
        } else if (i >= baseMagnification * 5 && i < baseMagnification * 6) {
            // NO.3.5
            return Color.parseColor("#64C4B1");
        } else if (i >= baseMagnification * 6 & i < baseMagnification * 7) {
            // NO.4
            return Color.parseColor("#6dc393");
        } else if (i >= baseMagnification * 7 & i < baseMagnification * 8) {
            // NO.4.5
            return Color.parseColor("#6DC16C");
        } else if (i >= baseMagnification * 8 & i < baseMagnification * 9) {
            // NO.5
            return Color.parseColor("#6dbe45");
        } else if (i >= baseMagnification * 9 & i < baseMagnification * 10) {
            // NO.5.5
            return Color.parseColor("#93C93B");
        } else if (i >= baseMagnification * 10 & i < baseMagnification * 11) {
            // NO.6
            return Color.parseColor("#bbd631");
        } else if (i >= baseMagnification * 11 & i < baseMagnification * 12) {
            // NO.6.5
            return Color.parseColor("#CEDD27");
        } else if (i >= baseMagnification * 12 & i < baseMagnification * 13) {
            // NO.7
            return Color.parseColor("#e1e31a");
        } else if (i >= baseMagnification * 13 & i < baseMagnification * 14) {
            // NO.7.5
            return Color.parseColor("#EEE711");
        } else if (i >= baseMagnification * 14 & i < baseMagnification * 15) {
            // NO.8
            return Color.parseColor("#fdee09");
        } else if (i >= baseMagnification * 15 & i < baseMagnification * 16) {
            // NO.8.5
            return Color.parseColor("#FEDD06");
        } else if (i >= baseMagnification * 16 & i < baseMagnification * 17) {
            // NO.9
            return Color.parseColor("#ffcd04");
        } else if (i >= baseMagnification * 17 & i < baseMagnification * 18) {
            // NO.9.5
            return Color.parseColor("#FDBE12");
        } else if (i >= baseMagnification * 18 & i < baseMagnification * 19) {
            // NO.10
            return Color.parseColor("#fbaa19");
        } else if (i >= baseMagnification * 19 & i < baseMagnification * 20) {
            // NO.10.5
            return Color.parseColor("#F9991A");
        } else if (i >= baseMagnification * 20 & i < baseMagnification * 21) {
            // NO.11
            return Color.parseColor("#f68b1f");
        } else if (i >= baseMagnification * 21 & i < baseMagnification * 22) {
            // NO.11.5
            return Color.parseColor("#F36E20");
        } else if (i >= baseMagnification * 22 & i < baseMagnification * 23) {
            // NO.12
            return Color.parseColor("#f04e23");
        } else if (i >= baseMagnification * 23 & i < baseMagnification * 24.5) {
            // NO.12.5
            return Color.parseColor("#EF3925");
        } else if (i >= baseMagnification * 24.5) {
            // NO.13
            return Color.parseColor("#ed2024");
        } else {
            return Color.parseColor("#ffffff");
        }
    }

}
