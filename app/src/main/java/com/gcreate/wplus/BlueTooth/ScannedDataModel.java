package com.gcreate.wplus.BlueTooth;

import android.bluetooth.BluetoothDevice;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ScannedDataModel {

    /**這邊是拿取掃描到的所有資訊*/
    private BluetoothDevice device;
    private String deviceName;
    private String rssi;
    private String deviceByteInfo;
    private String address;



    public ScannedDataModel(BluetoothDevice device, String deviceName, String rssi, String deviceByteInfo, String address) {
        this.device = device;
        this.deviceName = deviceName;
        this.rssi = rssi;
        this.deviceByteInfo = deviceByteInfo;
        this.address = address;
    }

    public BluetoothDevice getDevice() {
        return device;
    }


    public String getAddress() {
        return address;
    }

    public String getRssi() {
        return rssi;
    }

    public String getDeviceByteInfo() {
        return deviceByteInfo;
    }

    public String getDeviceName() {
        return deviceName;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        com.gcreate.wplus.BlueTooth.ScannedDataModel p = (com.gcreate.wplus.BlueTooth.ScannedDataModel)obj;

        return this.address.equals(p.address);
    }

    @NonNull
    @Override
    public String toString() {
        return this.address;
    }
}
