package com.gcreate.wplus.BlueTooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.gcreate.wplus.R;
import com.gcreate.wplus.databinding.ActivitySearchBlueToothBinding;
import com.gcreate.wplus.global;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Activity_SearchBlueTooth extends AppCompatActivity {

    //------------------------------- View Parameter ------------------------------------
    private ActivitySearchBlueToothBinding binding;
    private static String tmpDevice;
    private RecyclerViewAdapter mAdapter;

    //------------------------------- BlueTooth Parameter ------------------------------------
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    public static BluetoothGatt bluetoothGatt;
    private final ArrayList<ScannedDataModel> findDevice = new ArrayList<>();
    private static BluetoothDevice bluetoothDevice;

    //------------------------------- Other Parameter ------------------------------------
    public static Map<Integer, String> map = new HashMap<>();
    private static final String TAG = "WIFI_BLE";
    private boolean isScanning = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_blue_tooth);

        initVIew();
        /* 初始藍牙掃描及掃描開關之相關功能 */
        bluetoothScan();

    }

    private void initVIew() {
        binding.searchToolbar.setNavigationOnClickListener(v -> onBackPressed());
        binding.recyclerViewScannedList.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new RecyclerViewAdapter();

        if (bluetoothGatt != null) {
            binding.tvCurrentConnectedDevice.setText(tmpDevice);
        }

    }

    private void bluetoothScan() {
        /*  Initializes Bluetooth adapter.
         * 啟用藍牙適配器*/
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        /*設置Recyclerview列表*/
        binding.recyclerViewScannedList.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // 針對BLE 裝置連線GATT  服務
                if (!isScanning) {
                    if (bluetoothGatt != null) {
                        bluetoothGatt.disconnect();
                        bluetoothGatt.close();
                        bluetoothGatt = null;
                    }
                    bluetoothDevice = newList.get(position).getDevice();
                    global.bluetoothDevice = bluetoothDevice;
                    bluetoothGatt = bluetoothDevice.connectGatt(getApplicationContext(), false, bluetoothGattCallback);
                    tmpDevice = String.format("%s%s", getResources().getString(R.string.bluetooth_current_connected), newList.get(position).getAddress());
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.bluetooth_notice), Toast.LENGTH_LONG).show();
                }

            }
        });

        /*製作停止/開始掃描的按鈕*/
        binding.buttonScan.setOnClickListener((v) -> {
            if (isScanning) {
                /*關閉掃描*/
                isScanning = false;
                binding.buttonScan.setText(R.string.bluetooth_start_scan);
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            } else {
                /*開啟掃描*/
                isScanning = true;
                binding.buttonScan.setText(R.string.bluetooth_stop_scan);
                findDevice.clear();
                mBluetoothAdapter.startLeScan(mLeScanCallback);
                mAdapter.clearDevice();

            }
        });
    }

    //--------------------------------------------------BlueTooth Search ---------------------------------------------------------------------------------
    public static List<ScannedDataModel> newList;
    /**
     * 顯示掃描到物件
     */
    private final BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            new Thread(() -> {
                /*如果裝置沒有名字，就不顯示*/
                if (device.getName() != null) {
                    /*將搜尋到的裝置加入陣列*/
                    findDevice.add(new ScannedDataModel(device, device.getName()
                            , String.valueOf(rssi)
                            , byteArrayToHexStr(scanRecord)
                            , device.getAddress()));
                    /*將陣列中重複Address的裝置濾除，並使之成為最新數據*/
                    newList = getSingle(findDevice);

                    runOnUiThread(() -> {
                        /*將陣列送到RecyclerView列表中*/
                        mAdapter.addDevice(newList);
                    });
                }
            }).start();
        }
    };


    /**
     * 濾除重複的藍牙裝置(以Address判定)
     */
    private ArrayList<ScannedDataModel> getSingle(ArrayList<ScannedDataModel> list) {
        ArrayList<ScannedDataModel> tempList = new ArrayList<>();
        try {
            for (ScannedDataModel obj : list) {
                if (!tempList.contains(obj)) {
                    tempList.add(obj);

                } else {
                    tempList.set(getIndex(tempList, obj), obj);
                }
            }
            return tempList;
        } catch (ConcurrentModificationException e) {
            return tempList;
        }
    }

    /**
     * 以Address篩選陣列->抓出該值在陣列的哪處
     */
    private int getIndex(ArrayList<ScannedDataModel> temp, Object obj) {
        for (int i = 0; i < temp.size(); i++) {
            if (temp.get(i).toString().contains(obj.toString())) {
                return i;
            }
        }
        return -1;
    }


    /**
     * 避免跳轉後掃描程序係續浪費效能，因此離開頁面後即停止掃描
     */
    @Override
    protected void onStop() {
        super.onStop();
        final Button btScan = findViewById(R.id.button_Scan);
        //  關閉掃描
        isScanning = false;
        btScan.setText(R.string.bluetooth_start_scan);
        mBluetoothAdapter.stopLeScan(mLeScanCallback);
    }

    //-----------------------------------------------------------------BLE GATT Function-------------------------------------------------------------------------------------------------------
    private StringBuffer tmpString = new StringBuffer();
    private int BTDeviceFeedBackRow = 0;

    /**
     * 通過BLE API的不同類型的連接方法
     */
    private final BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback() {
        //  當連接狀態發生改變
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                // 連接成功後啟動服務
                Log.d(TAG, "onConnectionStateChange 連接成功");
                //查找服務
                bluetoothGatt.discoverServices();
            } else if (newState == BluetoothProfile.STATE_CONNECTING) {
                Log.d(TAG, "onConnectionStateChange 連接中......");
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.d(TAG, "onConnectionStateChange 連接失敗");
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.bluetooth_connect_fail), Toast.LENGTH_SHORT).show();
                binding.tvCurrentConnectedDevice.setText("目前連線裝置為 : ");
                feedBackResult(false);
                gatt.disconnect();
                gatt.close();
            } else if (newState == BluetoothProfile.STATE_DISCONNECTING) {
                Log.d(TAG, "onConnectionStateChange 連接斷開中......");
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.bluetooth_connect_fail), Toast.LENGTH_SHORT).show();
                binding.tvCurrentConnectedDevice.setText("目前連線裝置為 : ");
                gatt.disconnect();
                gatt.close();
            }
        }

        // 服務 uuid
        // 當發現新的服務器  or 服務的回调
        private final UUID UUID_SERVER = UUID.fromString("0000fe50-0000-1000-8000-00805f9b34fb");
        private final UUID UUID_Characteristic = UUID.fromString("0000fe51-0000-1000-8000-00805f9b34fb");

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            //设置读特征值的监听，接收服务端发送的数

            if (status == BluetoothGatt.GATT_SUCCESS) {
                BluetoothGattService service = bluetoothGatt.getService(UUID_SERVER);
                BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID_Characteristic);
                global.characteristic = characteristic;
                bluetoothGatt.setCharacteristicNotification(characteristic, true);
                feedBackResult(true);
            } else {
                Log.e(TAG, "GATT服務發現失敗，錯誤代碼:" + status);
            }
        }


        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);

            byte[] bytes = characteristic.getValue();
            StringBuffer hexString = byteArrayToHexStrBuffer(bytes);

            /* Log device signal
             * 大約3秒回傳所有的點
             */
            Log.d("BT response", "hexString :" + hexString);

            if (hexString.substring(0, 4).equals("0103")) {
                tmpString.append(hexString);
                BTDeviceFeedBackRow++;
            } else {
                if (BTDeviceFeedBackRow == 1 || BTDeviceFeedBackRow == 2) {
                    tmpString.append(hexString);
                    BTDeviceFeedBackRow++;
                } else if (BTDeviceFeedBackRow == 3) {
                    tmpString.append(hexString);
                    map.put(Integer.valueOf(tmpString.substring(4, 6), 16), tmpString.substring(6, tmpString.length() - 4));
                    tmpString.delete(0, tmpString.length());
                    BTDeviceFeedBackRow = 0;
                }
            }
        }
    };

    //-------------------------------------------------------   顯示裝置 GATT 服務連線結果  -------------------------------------------------------
    public void feedBackResult(boolean isConnected) {
        Activity_SearchBlueTooth.this.runOnUiThread(() -> {
            if (isConnected) {
                binding.tvCurrentConnectedDevice.setText(tmpDevice);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.bluetooth_connect_success), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.bluetooth_connect_fail), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Byte轉16進字串工具
     */
    public static StringBuffer byteArrayToHexStrBuffer(byte[] byteArray) {
        if (byteArray == null) {
            return null;
        }

        StringBuffer hex = new StringBuffer(byteArray.length * 2);
        for (byte aData : byteArray) {
            hex.append(String.format("%02X", aData));
        }

        return hex;
    }

    /**
     * Byte轉16進字串工具
     */
    public static String byteArrayToHexStr(byte[] byteArray) {

        if (byteArray == null) {
            return null;
        }

        StringBuilder hex = new StringBuilder(byteArray.length * 2);
        for (byte aData : byteArray) {
            hex.append(String.format("%02X", aData));
        }

        return hex.toString();
    }

}