package com.gcreate.wplus.BlueTooth;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gcreate.wplus.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<ScannedDataModel> arrayList = new ArrayList<>();
    private OnItemClickListener onItemClickListener = null;

    /**
     * 清除搜尋到的裝置列表
     */
    public void clearDevice() {
        this.arrayList.clear();
        notifyDataSetChanged();
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.scanned_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvName.setText(arrayList.get(position).getDeviceName());
        holder.tvAddress.setText(holder.itemView.getResources().getString(R.string.recyclerItem_deviceID) + arrayList.get(position).getAddress());
        holder.tvInfo.setText(holder.itemView.getResources().getString(R.string.recyclerItem_deviceByteInfo) + arrayList.get(position).getDeviceByteInfo());
        holder.tvRssi.setText(holder.itemView.getResources().getString(R.string.recyclerItem_RSSI) + arrayList.get(position).getRssi());

        holder.itemView.setOnClickListener(v ->
                onItemClickListener.onItemClick(holder.itemView, position)
        );
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    /**
     * 若有不重複的裝置出現，則加入列表中
     */
    public void addDevice(List<ScannedDataModel> arrayList) {
        this.arrayList = arrayList;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvAddress, tvInfo, tvRssi;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.textView_DeviceName);
            tvAddress = itemView.findViewById(R.id.textView_Address);
            tvInfo = itemView.findViewById(R.id.textView_ScanRecord);
            tvRssi = itemView.findViewById(R.id.textView_Rssi);

        }
    }
}
