package com.gcreate.wplus;

import static android.content.Context.MODE_PRIVATE;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.DisplayMetrics;

import androidx.appcompat.app.AppCompatActivity;

import com.gcreate.wplus.Tool.TestDataModel;

public class global {

    public static int windowWidth;
    public static int windowHeight;
    public static int pressureSettingValue;
    public static TestDataModel hasCushionData, noCushionData;

    public static BluetoothGattCharacteristic characteristic;
    public static BluetoothDevice bluetoothDevice;

    public static void getWindowSize(Activity mActivity) {
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        global.windowHeight = dm.heightPixels;
        global.windowWidth = dm.widthPixels;
    }


    public static boolean getIsFirstTimeOpenApp(Context context) {
        return ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).getBoolean("isFirstTimeOpenApp", true);
    }

    public static void setIsFirstTimeOpenApp(Context context, boolean mode) {
        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putBoolean("isFirstTimeOpenApp", mode);
        editor.apply();
        editor.commit();
    }

}
