package com.gcreate.wplus.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.gcreate.wplus.BlueTooth.Activity_SearchBlueTooth;
import com.gcreate.wplus.MainActivity;
import com.gcreate.wplus.R;
import com.gcreate.wplus.Tool.ChangeLanguage.Constant;
import com.gcreate.wplus.Tool.ChangeLanguage.SPUtils;
import com.gcreate.wplus.databinding.ActivitySettingBinding;

public class Activity_setting extends AppCompatActivity {

    // 0 : Follow System, 1: English, 2 : Tradition Chinese
    private static int languageIndex = SPUtils.getInstance(Constant.SP_NAME).getInt(Constant.SP_USER_LANG);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivitySettingBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_setting);

        binding.btnBack.setOnClickListener(v -> finish());

        // 藍芽連線設定
        binding.btnBluetooth.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), Activity_SearchBlueTooth.class);
            startActivity(intent);
        });

        //  壓力值設定
        binding.btnPressure.setOnClickListener(v -> {
            DialogFragment pressureSetting = new DialogFragment_pressureSetting();
            pressureSetting.show(getSupportFragmentManager(), "pressure setting dialog");
        });

        //  圖形及影片路徑設定
        binding.btnGraph.setOnClickListener(v -> startActivity(new Intent(Activity_setting.this, Activity_graphSelector.class)));

        //  其他參數設定
        binding.btnOther.setOnClickListener(v -> {
            DialogFragment keyPassword = new DialogFragment_keyPassword();
            keyPassword.show(getSupportFragmentManager(), "key password dialog");
        });

        binding.btnChangeLanguage.setOnClickListener(v -> {

            //  initial = 0
            switch (languageIndex){
                case 0 :
                case 2:
                    languageIndex = 1 ;
                    changeLanguage(languageIndex);
                    break;
                case 1:
                    languageIndex = 2 ;
                    changeLanguage(languageIndex);
                    break;
            }
        });
    }

    private void changeLanguage(int language) {
        //将选择的language保存到SP中
        SPUtils.getInstance(Constant.SP_NAME).put(Constant.SP_USER_LANG, language);

        //重新启动Activity,并且要清空栈
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}