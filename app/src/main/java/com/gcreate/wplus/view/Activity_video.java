package com.gcreate.wplus.view;

import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.widget.MediaController;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.gcreate.wplus.R;
import com.gcreate.wplus.databinding.ActivityVideoBinding;

public class Activity_video extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        ActivityVideoBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_video);

        MediaController mediaController = new MediaController(this);
        binding.videoView.setMediaController(mediaController);

        // VideoView Full Screen
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) binding.videoView.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;
        binding.videoView.setLayoutParams(params);

  /*其他 sdcard 路徑的範例，
  videoView.setVideoURI(
   Uri.parse("file://" +
   Environment.getExternalStoragePublicDirectory(
   Environment.DIRECTORY_MOVIES) + "/testmovie.mp4"));
  */

        // Use local Path
        binding.videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video));
        binding.videoView.requestFocus();
        binding.videoView.start();

        // 循環播放
        binding.videoView.setOnPreparedListener(mp -> {
            mp.start();
            mp.setLooping(true);
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent evet) {
        this.finish();
        return true;
    }

}