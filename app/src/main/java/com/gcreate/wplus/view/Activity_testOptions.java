package com.gcreate.wplus.view;

import static com.gcreate.wplus.BlueTooth.Activity_SearchBlueTooth.bluetoothGatt;
import static com.gcreate.wplus.global.characteristic;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.gcreate.wplus.BlueTooth.Activity_SearchBlueTooth;
import com.gcreate.wplus.MainActivity;
import com.gcreate.wplus.R;
import com.gcreate.wplus.Tool.StorageDataMaintain;
import com.gcreate.wplus.databinding.ActivityTestOptionsBinding;
import com.gcreate.wplus.global;

/**
 * 測試選項
 */
public class Activity_testOptions extends AppCompatActivity {

    private ActivityTestOptionsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_test_options);

        toolbarAction();

        binding.fixedColumnTvNoCushion.getLayoutParams().width = (int) (global.windowWidth * 0.45);
        binding.fixedColumnTvNoCushion.getLayoutParams().height = (int) (global.windowWidth * 0.1);
        binding.fixedColumnTvHasCushion.getLayoutParams().width = (int) (global.windowWidth * 0.45);
        binding.fixedColumnTvHasCushion.getLayoutParams().height = (int) (global.windowWidth * 0.1);
        binding.fixedColumnTvCheck.getLayoutParams().width = (int) (global.windowWidth * 0.26);
        binding.fixedColumnTvCheck.getLayoutParams().height = (int) (global.windowWidth * 0.12);

        // 無坐墊
        binding.fixedColumnTvNoCushion.setOnClickListener(v -> {
            binding.checkboxNoCushion.setChecked(true);
            /* 像裝置打訊號*/
            startBLE();
            Intent intent = new Intent(Activity_testOptions.this, Activity_test.class);
            intent.putExtra("HasCushion", false);
            startActivity(intent);
        });

        // 有坐墊
        binding.fixedColumnTvHasCushion.setOnClickListener(v -> {
            if (binding.checkboxNoCushion.isChecked()) {
                binding.checkboxHasCushion.setChecked(true);
                /* 像裝置打訊號*/
                startBLE();
                Intent intent = new Intent(Activity_testOptions.this, Activity_test.class);
                intent.putExtra("HasCushion", true);
                startActivity(intent);
            } else {
                Toast.makeText(Activity_testOptions.this, getResources().getString(R.string.test_option_notice_1), Toast.LENGTH_SHORT).show();
            }
        });

        binding.fixedColumnTvCheck.setOnClickListener(v -> {
            stopBLE();
            if (binding.checkboxNoCushion.isChecked() && binding.checkboxHasCushion.isChecked()) {
                startActivity(new Intent(Activity_testOptions.this, Activity_finalReport.class));
            } else {
                Toast.makeText(Activity_testOptions.this, getResources().getString(R.string.test_option_notice_2), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void toolbarAction() {
        binding.testOptionsToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        binding.testOptionsToolbar.imgHome.setOnClickListener(v -> startActivity(new Intent(Activity_testOptions.this, MainActivity.class)));

        setImages();
    }

    private void setImages() {

        //  WPLUS  商標 LOGO
        Glide.with(this).load(Uri.parse(StorageDataMaintain.uriPathList.get(0))).error(R.mipmap.logo_main).centerInside().into(binding.imgLogoMain);

        // 推廣標籤
        Glide.with(this).load(Uri.parse(StorageDataMaintain.uriPathList.get(2))).error(R.mipmap.barcode).centerInside().into(binding.imgPromotionLabel);

    }

    private void startBLE() {
        String startListen = "01030000F1D8";
        if (global.characteristic != null) {
            byte[] b = hexStringToByteArray(startListen);
            global.characteristic.setValue(b);
            Activity_SearchBlueTooth.bluetoothGatt.writeCharacteristic(global.characteristic);
        }
    }

    @SuppressLint("MissingPermission")
    private void stopBLE() {
        String stopListen = "01060000E1D9";
        if (characteristic != null) {
            byte[] b = hexStringToByteArray(stopListen);
            characteristic.setValue(b);
            bluetoothGatt.writeCharacteristic(characteristic);
        }
    }


    private byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

}