package com.gcreate.wplus.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.gcreate.wplus.R;
import com.gcreate.wplus.Tool.StorageDataMaintain;
import com.gcreate.wplus.Tool.UserInfoModel;
import com.gcreate.wplus.databinding.ActivityContactInfomationBinding;
import com.gcreate.wplus.global;

/**
 * 測試者個人資料
 */
public class Activity_contactInformation extends AppCompatActivity {

    private ActivityContactInfomationBinding binding;
    public static UserInfoModel userdata = new UserInfoModel("", "女士", "", "", "");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contact_infomation);

        if (getIntent().getExtras() != null) {
            resetUserdata();
        }

        initView();
        toolbarAction();

        binding.userInfoDone.setOnClickListener(v -> {

            String Name = binding.editUserName.getText().toString();
            String BYear = binding.editUserBirthdayYear.getText().toString();
            String BMonth = binding.editUserBirthdayMonth.getText().toString();
            String Phone = binding.editUserCellphone.getText().toString();
            String WeChat = binding.editUserWeChat.getText().toString();
            if (Name.trim().isEmpty() || BYear.trim().isEmpty() || BMonth.trim().isEmpty() || Phone.trim().isEmpty() || WeChat.trim().isEmpty()) {
                Toast.makeText(this, getResources().getString(R.string.userInfo_notice), Toast.LENGTH_SHORT).show();
            } else {
                saveUserdata();
                startActivity(new Intent(Activity_contactInformation.this, Activity_testOptions.class));
            }
        });
    }

    private void toolbarAction() {
        binding.userInfoToolbar.toolbar.setNavigationOnClickListener(v -> finish());
        binding.userInfoToolbar.imgHome.setOnClickListener(v -> {
            resetUserdata();
            finish();
        });
    }

    private void initView() {
        binding.editUserName.setText(userdata.getUserName());
        binding.userInfoDone.getLayoutParams().width = (int) (global.windowWidth * 0.2);
        binding.userInfoDone.getLayoutParams().height = (int) (global.windowWidth * 0.1);

        if (userdata.getUserGender().equals("女士")) {
            binding.mRadioFemale.setChecked(true);
        } else if (userdata.getUserGender().equals("男士")) {
            binding.mRadioMale.setChecked(true);
        }

        if (userdata.getUserBirthday().trim().isEmpty() || userdata.getUserBirthday().trim().equals("-")) {
            binding.editUserBirthdayYear.setText("");
            binding.editUserBirthdayMonth.setText("");
        } else {
            binding.editUserBirthdayYear.setText(userdata.getUserBirthday().split("-")[0]);
            binding.editUserBirthdayMonth.setText(userdata.getUserBirthday().split("-")[1]);
        }

        binding.editUserCellphone.setText(userdata.getUserCellphone());
        binding.editUserWeChat.setText(userdata.getWeChat());
        setImages();
    }

    private void setImages() {

        //  WPLUS  商標 LOGO
        Glide.with(this).load(Uri.parse(StorageDataMaintain.uriPathList.get(0))).error(R.mipmap.logo_main).centerInside().into(binding.imgLogoMain);

        // 推廣標籤
        Glide.with(this).load(Uri.parse(StorageDataMaintain.uriPathList.get(2))).error(R.mipmap.barcode).centerInside().into(binding.imgPromotionLabel);

    }

    private void resetUserdata() {
        userdata.setUserName("");
        userdata.setUserGender("女士");
        binding.mRadioFemale.setChecked(true);
        userdata.setUserBirthday("");
        userdata.setUserCellphone("");
        userdata.setWeChat("");
    }

    private void saveUserdata() {
        userdata.setUserName(binding.editUserName.getText().toString());
        if (binding.mRadioFemale.isChecked()) {
            userdata.setUserGender("女士");
        } else if (binding.mRadioMale.isChecked()) {
            userdata.setUserGender("男士");
        }
        userdata.setUserBirthday(binding.editUserBirthdayYear.getText() + "-" + binding.editUserBirthdayMonth.getText());
        userdata.setUserCellphone(binding.editUserCellphone.getText().toString());
        userdata.setWeChat(binding.editUserWeChat.getText().toString());
    }

}