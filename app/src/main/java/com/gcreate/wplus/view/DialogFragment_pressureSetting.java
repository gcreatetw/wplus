package com.gcreate.wplus.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.gcreate.wplus.MainActivity;
import com.gcreate.wplus.R;
import com.gcreate.wplus.Tool.StorageDataMaintain;
import com.gcreate.wplus.databinding.DfPressureSettingBinding;
import com.gcreate.wplus.global;


public class DialogFragment_pressureSetting extends DialogFragment {

    private DfPressureSettingBinding binding;

    public DialogFragment_pressureSetting() {
    }

    public void onStart() {
        super.onStart();
        //  設定 Dialog 視窗寬高
        if (getDialog() != null) {
            getDialog().getWindow().setLayout((int) (global.windowWidth * 0.8), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater,R.layout.df_pressure_setting,container,false);
        binding.pressureSeekBar.setProgress(StorageDataMaintain.getPressureSettingValue(MainActivity.mContext));
        binding.tvSeekBarTxt.setText(String.valueOf(StorageDataMaintain.getPressureSettingValue(MainActivity.mContext)));

        binding.pressureSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                binding.tvSeekBarTxt.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        StorageDataMaintain.setPressureSettingValue(MainActivity.mContext, Integer.parseInt(binding.tvSeekBarTxt.getText().toString()));
    }
}