package com.gcreate.wplus.view;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.gcreate.wplus.R;
import com.gcreate.wplus.Tool.StorageDataMaintain;
import com.gcreate.wplus.databinding.DfKeyPasswordBinding;
import com.gcreate.wplus.global;

import java.util.Objects;

public class DialogFragment_keyPassword extends DialogFragment implements TextView.OnEditorActionListener {

    private DfKeyPasswordBinding binding;
    private InputMethodManager imm;

    public DialogFragment_keyPassword() {
    }

    public void onStart() {
        super.onStart();
        //  設定 Dialog 視窗寬高
        if (getDialog() != null) {
            getDialog().getWindow().setLayout((int) (global.windowWidth * 0.8), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.df_key_password, container, false);
        binding.editPassword.setOnEditorActionListener(this);
        imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);

        binding.btnOk.setOnClickListener(v -> {
            if (binding.editPassword.getText().toString().equals(StorageDataMaintain.getPassword(getActivity()))) {
                DialogFragment otherSetting = new DialogFragment_otherSetting();
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                binding.editPassword.clearFocus();
                binding.tvPasswordAlert.setText("");
                otherSetting.show(getActivity().getSupportFragmentManager(), "other setting dialog");
                getDialog().dismiss();
            } else {
                if (binding.editPassword.getText().toString().trim().isEmpty()) {
                    binding.tvPasswordAlert.setText(getResources().getString(R.string.setting_keyIn_notice));
                } else {
                    binding.tvPasswordAlert.setText(getResources().getString(R.string.setting_keyIn_incorrect));
                }
            }
        });

        binding.btnCancel.setOnClickListener(v -> Objects.requireNonNull(getDialog()).dismiss());

        binding.tvChangePassword.setOnClickListener(v -> {
            DialogFragment modifyPassword = new DialogFragment_modifyPassword();
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            binding.editPassword.clearFocus();
            binding.tvPasswordAlert.setText("");
            modifyPassword.show(getActivity().getSupportFragmentManager(), "modify password dialog");
        });

        return binding.getRoot();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        binding.editPassword.clearFocus();
        return true;
    }

}