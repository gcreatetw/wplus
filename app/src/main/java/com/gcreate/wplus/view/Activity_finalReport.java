package com.gcreate.wplus.view;

import static com.gcreate.wplus.view.Activity_test.maxPressureModel;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.gcreate.wplus.MainActivity;
import com.gcreate.wplus.R;
import com.gcreate.wplus.Tool.GeneratePDF;
import com.gcreate.wplus.Tool.GenerateTxt;
import com.gcreate.wplus.Tool.StorageDataMaintain;
import com.gcreate.wplus.databinding.ActivityFinalReportBinding;
import com.gcreate.wplus.global;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Activity_finalReport extends AppCompatActivity {

    private ActivityFinalReportBinding binding;
    private String fileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_final_report);

        toolbarAction();
        intView();
        viewAction();

    }

    private void toolbarAction() {
        binding.testOptionsToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        binding.testOptionsToolbar.imgHome.setOnClickListener(v -> startActivity(new Intent(Activity_finalReport.this, MainActivity.class)));
        setImages();
    }

    private void setImages() {
        //  WPLUS  商標 LOGO
        Glide.with(this).load(Uri.parse(StorageDataMaintain.uriPathList.get(0))).error(R.mipmap.logo_main).centerInside().into(binding.imgLogoMain);
    }

    @SuppressLint("SetTextI18n")
    private void intView() {

        //  普通坐墊
        float noCushionDataPressureIndex = global.noCushionData.getTestPressureIndex();
        //  BalanceOn 坐墊
        float hasCushionDataPressureIndex = global.hasCushionData.getTestPressureIndex();

        //  減壓度
        float DecompressionDegree = ((noCushionDataPressureIndex - hasCushionDataPressureIndex) / noCushionDataPressureIndex) * 100;
        DecimalFormat df = new DecimalFormat("##0.00");
        binding.tvTestResultDecompressionDegree.setText(df.format(DecompressionDegree) + "%");

        binding.imgNoCushion.setImageBitmap(global.noCushionData.getPressureGraph());
        binding.imgHasCushion.setImageBitmap(global.hasCushionData.getPressureGraph());

        binding.titleName.setText(String.format("%s %s", Activity_contactInformation.userdata.getUserName(), getResources().getString(R.string.final_report_name_value)));

        Date calendar = Calendar.getInstance().getTime();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat sdf2 = new SimpleDateFormat("YYMMdd-HHmm", Locale.getDefault());
        String currentDate = sdf1.format(calendar);
        String currentDateAndTime = sdf2.format(calendar);
        fileName = StorageDataMaintain.otherSettingList.get(0) + "-" + StorageDataMaintain.otherSettingList.get(1) + "-" + currentDateAndTime + ".pdf";

        binding.titleTestDate.setText(currentDate);
        binding.titleTestNo.setText(fileName);

        final String[] products = {
                "BalanceOn 便攜式健康座墊 42×35.5×2 cm (W×H×D)",
                "BalanceOn 蜂巢凝膠座墊 (中碼) 42×41×2.5 cm (W×H×D)",
                "BalanceOn 蜂巢凝膠健康座墊 (大碼) 47×42×2.5 cm (W×H×D)"};

        binding.productSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, products));
    }

    private void viewAction() {
        binding.productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    binding.imgFinalReportProduct.setBackgroundResource(R.mipmap.cushion_ss);
                } else if (position == 1) {
                    binding.imgFinalReportProduct.setBackgroundResource(R.mipmap.cushion_sm);
                } else if (position == 2) {
                    binding.imgFinalReportProduct.setBackgroundResource(R.mipmap.cushion_sl);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        binding.fixedColumnTvCheck.setOnClickListener(v -> {
            Toast.makeText(this, getResources().getString(R.string.final_report_notice), Toast.LENGTH_LONG).show();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    GenerateTxt.writeTxtToFile(Activity_finalReport.this, Activity_contactInformation.userdata);
                    GeneratePDF.generatePdf(binding.viewPreparePDF, fileName);
                }
            }).start();
            maxPressureModel.setMaxPressureValue("0");
            startActivity(new Intent(Activity_finalReport.this, MainActivity.class));
            finish();
        });
    }

}