package com.gcreate.wplus.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.gcreate.wplus.MainActivity;
import com.gcreate.wplus.R;
import com.gcreate.wplus.Tool.StorageDataMaintain;
import com.gcreate.wplus.databinding.DfOtherSettingBinding;
import com.gcreate.wplus.global;

public class DialogFragment_otherSetting extends DialogFragment {

    private DfOtherSettingBinding binding;

    public DialogFragment_otherSetting() {
    }

    public void onStart() {
        super.onStart();
        //  設定 Dialog 視窗寬高
        if (getDialog() != null) {
            getDialog().getWindow().setLayout((int) (global.windowWidth * 0.8), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.df_other_setting, container, false);

        binding.editArea.setText(StorageDataMaintain.otherSettingList.get(0));
        binding.editGroupNo.setText(StorageDataMaintain.otherSettingList.get(1));

        binding.btnOk.setOnClickListener(v -> {

            if (!binding.editGroupNo.getText().toString().isEmpty() && !binding.editArea.getText().toString().isEmpty()) {
                StorageDataMaintain.updateOtherSettingValue(MainActivity.mContext, 0, binding.editArea.getText().toString());
                StorageDataMaintain.updateOtherSettingValue(MainActivity.mContext, 1, binding.editGroupNo.getText().toString());
                Toast.makeText(getActivity(), getResources().getString(R.string.setting_other_finished), Toast.LENGTH_SHORT).show();
                getDialog().dismiss();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.setting_other_notice), Toast.LENGTH_SHORT).show();
            }

        });

        return binding.getRoot();
    }
}