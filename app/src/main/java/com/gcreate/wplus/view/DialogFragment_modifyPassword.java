package com.gcreate.wplus.view;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.gcreate.wplus.R;
import com.gcreate.wplus.Tool.StorageDataMaintain;
import com.gcreate.wplus.databinding.DfModifyPasswordBinding;
import com.gcreate.wplus.global;

import java.util.Objects;

public class DialogFragment_modifyPassword extends DialogFragment implements TextView.OnEditorActionListener {

    private DfModifyPasswordBinding binding;
    private InputMethodManager imm;

    public DialogFragment_modifyPassword() {
    }


    public void onStart() {
        super.onStart();
        //  設定 Dialog 視窗寬高
        if (getDialog() != null) {
            getDialog().getWindow().setLayout((int) (global.windowWidth * 0.8), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.df_modify_password, container, false);
        imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);

        binding.editNewPassword2.setOnEditorActionListener(this);

        binding.btnOk.setOnClickListener(v -> {

            String old_password = binding.editOldPassword.getText().toString();
            String new_password = binding.editNewPassword.getText().toString();
            String new_password_repeat = binding.editNewPassword2.getText().toString();

            if (old_password.equals(StorageDataMaintain.getPassword(getActivity())) && !old_password.trim().isEmpty()) {
                if (new_password.equals(new_password_repeat) && !new_password.trim().isEmpty() && !new_password_repeat.trim().isEmpty()) {
                    StorageDataMaintain.setPassword(getActivity(), binding.editNewPassword2.getText().toString());
                    imm.hideSoftInputFromWindow(Objects.requireNonNull(getView()).getWindowToken(), 0);
                    Objects.requireNonNull(getDialog()).dismiss();
                    Toast.makeText(getActivity(), getResources().getString(R.string.setting_change_password_success), Toast.LENGTH_SHORT).show();
                } else {
                    if (new_password.trim().isEmpty() || new_password_repeat.trim().isEmpty()) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.setting_change_password_newPassword_blank), Toast.LENGTH_SHORT).show();
                    } else if (!new_password.equals(new_password_repeat)) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.setting_change_password_two_newPassword_mismatch), Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                if (!old_password.equals(StorageDataMaintain.getPassword(getActivity()))) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.setting_change_password_oldPassword_incorrect), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.setting_change_password_fail), Toast.LENGTH_SHORT).show();
                }
            }
        });

        binding.btnCancel.setOnClickListener(v -> getDialog().dismiss());

        return binding.getRoot();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        binding.editNewPassword2.clearFocus();
        return true;
    }

}