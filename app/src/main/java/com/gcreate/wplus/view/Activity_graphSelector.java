package com.gcreate.wplus.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.gcreate.wplus.Listener.LogoListenerManager;
import com.gcreate.wplus.MainActivity;
import com.gcreate.wplus.R;
import com.gcreate.wplus.Tool.GetPathFromURI;
import com.gcreate.wplus.Tool.StorageDataMaintain;
import com.gcreate.wplus.databinding.ActivityGraphSelectorBinding;

public class Activity_graphSelector extends AppCompatActivity {

    private static final int SELECT_LOGO_MAIN_IMAGE = 100;
    private static final int SELECT_LOGO_SPONSOR_IMAGE = 101;
    private static final int SELECT_PROMOTION_LABEL_IMAGE = 102;
    private static final int SELECT_VIDEO = 104;

    private ActivityGraphSelectorBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_graph_selector);

        initView();
        btnClickAction();

    }

    private void initView() {
        if (StorageDataMaintain.realPathList.get(0) != null) {
            binding.realPathLogo.setText(StorageDataMaintain.realPathList.get(0));
        } else {
            binding.realPathLogo.setText("");
        }

        if (StorageDataMaintain.realPathList.get(1) != null) {
            binding.realPathSponsor.setText(StorageDataMaintain.realPathList.get(1));
        } else {
            binding.realPathSponsor.setText("");
        }

        if (StorageDataMaintain.realPathList.get(2) != null) {
            binding.realPathPromotionLabel.setText(StorageDataMaintain.realPathList.get(2));
        } else {
            binding.realPathPromotionLabel.setText("");
        }

        if (StorageDataMaintain.realPathList.get(3) != null) {
            binding.realPathVideo.setText(StorageDataMaintain.realPathList.get(3));
        } else {
            binding.realPathVideo.setText("");
        }
    }

    private void btnClickAction() {

        binding.btnBack.setOnClickListener(v -> finish());

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);


        binding.btnLogoMainSelector.setOnClickListener(v -> {
            if (checkPermission()) {
                intent.setType("image/*");
                startActivityForResult(intent, SELECT_LOGO_MAIN_IMAGE);
            } else {
                Toast.makeText(this, getResources().getString(R.string.permission_check_read_file), Toast.LENGTH_SHORT).show();
            }
        });

        binding.btnSponsorImageSelector.setOnClickListener(v -> {
            //检查权限
            if (checkPermission()) {
                intent.setType("image/*");
                startActivityForResult(intent, SELECT_LOGO_SPONSOR_IMAGE);
            } else {
                Toast.makeText(this, getResources().getString(R.string.permission_check_read_file), Toast.LENGTH_SHORT).show();
            }
        });

        binding.btnPromotionLabelSelector.setOnClickListener(v -> {
            //检查权限
            if (checkPermission()) {
                intent.setType("image/*");
                startActivityForResult(intent, SELECT_PROMOTION_LABEL_IMAGE);
            } else {
                Toast.makeText(this, getResources().getString(R.string.permission_check_read_file), Toast.LENGTH_SHORT).show();
            }
        });

        binding.btnVideoSelector.setOnClickListener(v -> {
            //检查权限
            if (checkPermission()) {
                intent.setType("video/*");
                startActivityForResult(intent, SELECT_VIDEO);
            } else {
                Toast.makeText(this, getResources().getString(R.string.permission_check_read_file), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            assert data != null;
            switch (requestCode) {
                case SELECT_LOGO_MAIN_IMAGE:
                    Uri Logo_main_Uri = data.getData();
                    // 儲存路徑 uri 需要先賦予權限
                    grantUriPermission(getPackageName(), Logo_main_Uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    final int takeFlags = data.getFlags() & Intent.FLAG_GRANT_READ_URI_PERMISSION;
                    assert Logo_main_Uri != null;
                    getContentResolver().takePersistableUriPermission(Logo_main_Uri, takeFlags);
                    // 存圖片URI
                    StorageDataMaintain.updateSingleUriPath(MainActivity.mContext, 0, Logo_main_Uri.toString());
                    // 存圖片 Real Path
                    String realPath = GetPathFromURI.getPathFromURI(this, Logo_main_Uri);
                    StorageDataMaintain.updateSingleImageRealPath(MainActivity.mContext, 0, realPath);

                    binding.realPathLogo.setText(realPath);
                    LogoListenerManager.getInstance().sendBroadCast(0);
                    break;

                case SELECT_LOGO_SPONSOR_IMAGE:
                    Uri sponsor_imageUri = data.getData();
                    // 儲存路徑 uri 需要先賦予權限
                    grantUriPermission(getPackageName(), sponsor_imageUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    final int sponsorFlags = data.getFlags() & Intent.FLAG_GRANT_READ_URI_PERMISSION;
                    assert sponsor_imageUri != null;
                    getContentResolver().takePersistableUriPermission(sponsor_imageUri, sponsorFlags);
                    // 存圖片URI
                    StorageDataMaintain.updateSingleUriPath(MainActivity.mContext, 1, sponsor_imageUri.toString());
                    // 存圖片 Real Path
                    String sponsorRealPath = GetPathFromURI.getPathFromURI(this, sponsor_imageUri);
                    StorageDataMaintain.updateSingleImageRealPath(MainActivity.mContext, 1, sponsorRealPath);

                    binding.realPathSponsor.setText(sponsorRealPath);
                    LogoListenerManager.getInstance().sendBroadCast(1);
                    break;

                case SELECT_PROMOTION_LABEL_IMAGE:
                    Uri promotion_lebelUri = data.getData();
                    // 儲存路徑 uri 需要先賦予權限
                    grantUriPermission(getPackageName(), promotion_lebelUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    final int promotionFlags = data.getFlags() & Intent.FLAG_GRANT_READ_URI_PERMISSION;
                    assert promotion_lebelUri != null;
                    getContentResolver().takePersistableUriPermission(promotion_lebelUri, promotionFlags);
                    // 存圖片URI
                    StorageDataMaintain.updateSingleUriPath(MainActivity.mContext, 2, promotion_lebelUri.toString());
                    // 存圖片 Real Path
                    String promotionRealPath = GetPathFromURI.getPathFromURI(this, promotion_lebelUri);
                    StorageDataMaintain.updateSingleImageRealPath(MainActivity.mContext, 2, promotionRealPath);

                    binding.realPathPromotionLabel.setText(promotionRealPath);
                    LogoListenerManager.getInstance().sendBroadCast(2);
                    break;

                case SELECT_VIDEO:
                    Uri videolUri = data.getData();
                    // 儲存路徑 uri 需要先賦予權限
                    grantUriPermission(getPackageName(), videolUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    final int videoFlags = data.getFlags() & Intent.FLAG_GRANT_READ_URI_PERMISSION;
                    assert videolUri != null;
                    getContentResolver().takePersistableUriPermission(videolUri, videoFlags);
                    // 存圖片URI
                    StorageDataMaintain.updateSingleUriPath(MainActivity.mContext, 3, videolUri.toString());
                    // 存圖片 Real Path
                    String videoRealPath = GetPathFromURI.getPathFromURI(this, videolUri);
                    StorageDataMaintain.updateSingleImageRealPath(MainActivity.mContext, 3, videoRealPath);

                    binding.realPathVideo.setText(videoRealPath);
                    LogoListenerManager.getInstance().sendBroadCast(3);

                    break;
            }
        }
    }

    private boolean checkPermission() {
        int hasGone2 = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return hasGone2 == PackageManager.PERMISSION_GRANTED;
    }
}
