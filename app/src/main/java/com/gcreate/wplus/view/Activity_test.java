package com.gcreate.wplus.view;

import static com.gcreate.wplus.BlueTooth.Activity_SearchBlueTooth.bluetoothGatt;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.gcreate.wplus.BlueTooth.CreateWplusImage;
import com.gcreate.wplus.MainActivity;
import com.gcreate.wplus.R;
import com.gcreate.wplus.Tool.StorageDataMaintain;
import com.gcreate.wplus.Tool.TestDataModel;
import com.gcreate.wplus.databinding.ActivityTestBinding;
import com.gcreate.wplus.global;
import com.gcreate.wplus.viewModel.MaxPressureValueModel;

public class Activity_test extends AppCompatActivity {

    private ActivityTestBinding binding;
    private final Handler handler = new Handler();
    private Runnable runnable;
    private boolean hasCushion;
    public static MaxPressureValueModel maxPressureModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_test);

        if (getIntent() != null) {
            // 無坐墊 False , 有坐墊 True
            hasCushion = getIntent().getBooleanExtra("HasCushion", false);
            initView(hasCushion);
            maxPressureModel = new MaxPressureValueModel();
        }

        binding.setViewModel(maxPressureModel);

        toolbarAction();
        viewAction();

        runnable = new Runnable() {
            @Override
            public void run() {
                binding.imgPressure.setImageBitmap(new CreateWplusImage(getApplicationContext()).getCurCanvas());
                handler.postDelayed(this, 800);
            }
        };

        handler.postDelayed(runnable, 0);

    }

    private void initView(boolean hasCushion) {
        if (hasCushion) {
            binding.ly3.setVisibility(View.INVISIBLE);
            binding.viewTitle.setText(getResources().getString(R.string.test_seating_cushion));
        } else {
            binding.ly3.setVisibility(View.VISIBLE);
            CreateWplusImage.dataMaxValue = 0;
            CreateWplusImage.getBTDataCount = 0;
            binding.viewTitle.setText(getResources().getString(R.string.test_standard_text));
        }

        binding.lyPressureArea.getLayoutParams().width = (int) (global.windowWidth * 0.4);
        binding.lyPressureArea.getLayoutParams().height = (int) (global.windowWidth * 0.1);
        binding.lyMeasureUnit.getLayoutParams().width = (int) (global.windowWidth * 0.4);
        binding.lyMeasureUnit.getLayoutParams().height = (int) (global.windowWidth * 0.1);
        binding.lyPressureIndex.getLayoutParams().width = (int) (global.windowWidth * 0.4);
        binding.lyPressureIndex.getLayoutParams().height = (int) (global.windowWidth * 0.1);
        binding.fixedColumnTvCheck.getLayoutParams().width = (int) (global.windowWidth * 0.2);
        binding.fixedColumnTvCheck.getLayoutParams().height = (int) (global.windowWidth * 0.1);
    }

    private void toolbarAction() {
        binding.testToolbar.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        binding.testToolbar.imgHome.setOnClickListener(v -> {
            //stopBLE();
            if (bluetoothGatt != null) {
                handler.removeCallbacks(runnable);
            }
            startActivity(new Intent(Activity_test.this, MainActivity.class));
        });
        setImages();
    }

    private void viewAction() {

        binding.btnResetMaxPressureValue.setOnClickListener(v -> {
            handler.removeCallbacks(runnable);
            CreateWplusImage.dataMaxValue = 0;
            CreateWplusImage.getBTDataCount = 0;
            maxPressureModel.setMaxPressureValue("0");
            handler.postDelayed(runnable, 500);
        });

        binding.fixedColumnTvCheck.setOnClickListener(v -> {
            // 儲存圖片、受壓面積、壓力指數、受壓度
            Bitmap bitmap = ((BitmapDrawable) binding.imgPressure.getDrawable()).getBitmap();
            binding.imgPressure.buildDrawingCache(true);

            int testPressureArea = Integer.parseInt(maxPressureModel.getPressureArea());
            float testPressureIndex = Float.parseFloat(maxPressureModel.getPressureIndex());
            float textCompression = Float.parseFloat(maxPressureModel.getCompression());

            if (hasCushion) {
                // 有坐墊的資料
                Bitmap hasCushionBitmap = ((BitmapDrawable) binding.imgPressure.getDrawable()).getBitmap();
                global.hasCushionData = new TestDataModel(hasCushionBitmap, testPressureArea, testPressureIndex, textCompression);
            } else {
                // 無坐墊的資料
                Bitmap noCushionBitmap = ((BitmapDrawable) binding.imgPressure.getDrawable()).getBitmap();
                global.noCushionData = new TestDataModel(noCushionBitmap, testPressureArea, testPressureIndex, textCompression);
            }

            finish();
        });
    }

    private void setImages() {
        // 商標
        if (StorageDataMaintain.uriPathList.get(0).isEmpty())
            Glide.with(this).load(R.mipmap.logo_main).centerInside().into(binding.imgLogoMain);
        else
            Glide.with(this).load(Uri.parse(StorageDataMaintain.uriPathList.get(0))).centerInside().into(binding.imgLogoMain);
    }


    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
        //stopBLE();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (bluetoothGatt != null) {
            handler.removeCallbacks(runnable);
        }
    }

}