package com.gcreate.wplus;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.gcreate.wplus.Listener.LogoListener;
import com.gcreate.wplus.Listener.LogoListenerManager;
import com.gcreate.wplus.Tool.Advertising;
import com.gcreate.wplus.Tool.ChangeLanguage.Constant;
import com.gcreate.wplus.Tool.ChangeLanguage.LangUtils;
import com.gcreate.wplus.Tool.ChangeLanguage.SPUtils;
import com.gcreate.wplus.Tool.StorageDataMaintain;
import com.gcreate.wplus.databinding.ActivityMainBinding;
import com.gcreate.wplus.view.Activity_contactInformation;
import com.gcreate.wplus.view.Activity_setting;
import com.gcreate.wplus.view.Activity_video;

public class MainActivity extends AppCompatActivity implements LogoListener {

    @SuppressLint("StaticFieldLeak")
    public static Context mContext;

    private ActivityMainBinding binding;
    private static boolean isFirstInActivity = true;


    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private static final int REQUEST_FINE_LOCATION_PERMISSION = 102;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION = 103;
    private Handler advertisingHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        LogoListenerManager.getInstance().registerListener(this);

        initStorageData();
        initView();
        requestPermission();

        //  閒置時播廣告
        advertisingHandler = new Advertising().advertisingHandler(getApplicationContext(), this, Activity_video.class);
        advertisingHandler.sendEmptyMessageDelayed(1, 60000); // 1分鐘後發送送消息

    }

    private void initStorageData() {
        mContext = this;
        //-------------------------------------    Init FavStockGroupList      --------------------------------
        if (global.getIsFirstTimeOpenApp(mContext)) {
            StorageDataMaintain.initPathArray(mContext);
            global.setIsFirstTimeOpenApp(mContext, false);
            StorageDataMaintain.getPressureSettingValue(mContext);
        } else {
            StorageDataMaintain.getImageRealPath(mContext);
            StorageDataMaintain.getUriPath(mContext);
            StorageDataMaintain.getOtherSettingValue(mContext);
            global.pressureSettingValue = StorageDataMaintain.getPressureSettingValue(mContext);
        }
        global.getWindowSize(this);
    }

    private void initView() {

        setImages();
        setTextLayout();
        binding.fixedColumnTvSetting.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, Activity_setting.class)));
        binding.fixedColumnTvStart.setOnClickListener(v -> {
            if (global.bluetoothDevice == null) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.main_notice), Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(MainActivity.this, Activity_contactInformation.class);
                intent.putExtra("isComeFromMainActivity", true);
                startActivity(intent);
            }
        });
    }

    private void setImages() {

        //  WPLUS  LOGO
        Glide.with(this).load(Uri.parse(StorageDataMaintain.uriPathList.get(0))).error(R.mipmap.logo_main).centerInside().into(binding.imgLogoMain);

        // 贊助商商標
        Glide.with(this).load(Uri.parse(StorageDataMaintain.uriPathList.get(1))).error(R.mipmap.logo_sponsor).centerInside().into(binding.imgLogoSponsor);

        // 推廣標籤
        Glide.with(this).load(Uri.parse(StorageDataMaintain.uriPathList.get(2))).error(R.mipmap.barcode).centerInside().into(binding.imgPromotionLabel);

    }

    private void requestPermission() {
        /*確認是否已開啟取得手機位置功能以及權限*/
        int hasGone = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasGone != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_FINE_LOCATION_PERMISSION);
        }
        /*確認手機是否支援藍牙BLE*/
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, getResources().getString(R.string.main_no_support_bluetooth), Toast.LENGTH_SHORT).show();
            finish();
        }
        /*開啟藍芽適配器*/
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 2);
        }

        int hasGone2 = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (hasGone2 != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION);
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        advertisingHandler.removeMessages(1); // 移除这个消息队列(重新计时)
        advertisingHandler.sendEmptyMessageDelayed(1, 60000 * 5);//   5分鐘後發送送消息
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        advertisingHandler.removeMessages(1); // 移除这个消息队列(重新计时)
        if (isFirstInActivity) {
            isFirstInActivity = false;
            advertisingHandler.sendEmptyMessageDelayed(1, 60000);//   1分鐘後發送送消息
        } else {
            advertisingHandler.sendEmptyMessageDelayed(1, 60000 * 5);//   5分鐘後發送送消息
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        advertisingHandler.removeMessages(1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogoListenerManager.getInstance().unRegisterListener(this);
    }

    @Override
    public void notifyViewChange(int position) {

        if (position == 0) {
            Glide.with(this).load(Uri.parse(StorageDataMaintain.uriPathList.get(0))).centerInside().into(binding.imgLogoMain);
        } else if ((position == 1)) {
            Glide.with(this).load(Uri.parse(StorageDataMaintain.uriPathList.get(1))).centerInside().into(binding.imgLogoSponsor);
        } else if ((position == 2)) {
            Glide.with(this).load(Uri.parse(StorageDataMaintain.uriPathList.get(2))).centerInside().into(binding.imgPromotionLabel);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case REQUEST_FINE_LOCATION_PERMISSION:
                int hasGone2 = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (hasGone2 != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION);
                }
                break;
            case REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION:

                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LangUtils.getAttachBaseContext(newBase, SPUtils.getInstance(Constant.SP_NAME).getInt(Constant.SP_USER_LANG)));
    }

    private void setTextLayout() {
        binding.fixedColumnTvSetting.getLayoutParams().width = (int) (global.windowWidth * 0.25);
        binding.fixedColumnTvSetting.getLayoutParams().height = (int) (global.windowWidth * 0.12);
        binding.fixedColumnTvStart.getLayoutParams().width = (int) (global.windowWidth * 0.25);
        binding.fixedColumnTvStart.getLayoutParams().height = (int) (global.windowWidth * 0.12);
    }

}