package com.gcreate.wplus.viewModel;

import android.icu.text.DecimalFormat;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.gcreate.wplus.BR;

public class MaxPressureValueModel extends BaseObservable {

    private String maxPressureValue = "0"; // 壓力最大值
    private String pressureArea = "0";    // 受壓面積
    private float pressureIndex = 0;   // 壓力指數
    private String compression = "0";     // 受壓度


    @Bindable
    public String getMaxPressureValue() {
        return maxPressureValue;
    }

    public void setMaxPressureValue(String mText) {
        this.maxPressureValue = mText;
        notifyPropertyChanged(BR.maxPressureValue);
    }


    @Bindable
    public String getPressureArea() {
        return pressureArea;
    }

    public void setPressureArea(String pressureArea) {
        this.pressureArea = pressureArea;
        notifyPropertyChanged(BR.pressureArea);
    }


    @Bindable
    public String getPressureIndex() {
        DecimalFormat df = new DecimalFormat("##0.0");
        return df.format(pressureIndex);
    }

    public void setPressureIndex(float pressureIndex) {
        this.pressureIndex = pressureIndex;
        notifyPropertyChanged(BR.pressureIndex);
    }

    @Bindable
    public String getCompression() {
        return compression;
    }

    public void setCompression(String compression) {
        this.compression = compression;
        notifyPropertyChanged(BR.compression);
    }
}
