package com.gcreate.wplus.Listener;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Android用觀察者模式代替廣播通知刷新界面
 * Reference : https://blog.csdn.net/csm_qz/article/details/46461651
 */

public class LogoListenerManager {
    /**
     * 單例模式
     */
    public static LogoListenerManager listenerManager;

    /**
     * 注册的接口集合，發送廣播的時候都能收到
     */
    private List<LogoListener> iListenerList = new CopyOnWriteArrayList<>();

    /**
     * 獲得單例對象對象
     */
    public static LogoListenerManager getInstance() {
        if (listenerManager == null) {
            listenerManager = new LogoListenerManager();
        }
        return listenerManager;
    }

    /**
     * 注册監聽
     */
    public void registerListener(LogoListener iListener) {
        iListenerList.add(iListener);
    }

    /**
     * 註銷監聽
     */
    public void unRegisterListener(LogoListener iListener) {
        if (iListenerList.contains(iListener)) {
            iListenerList.remove(iListener);
        }
    }

    /**
     * 發送廣播
     */
    public void sendBroadCast(int whichImage) {
        for (LogoListener iListener : iListenerList) {
            iListener.notifyViewChange(whichImage);
        }
    }


}

